var Q = require('q'),
    dam = require('./dam');

/**
 *
 *
 * @param externalIds Object where the keys are id types (ie 'cscookie') and the values are actual ids for the type
 *                      specified by the key.
 * @param callback function(error,internalId,internalIdsToMerge,isSuspicious)
 */
function getInternalUserId(externalIds) {
    //first, find all external ids that have a matching internal id
    var matchedExternalIdTypesToInternalIds = dam.findAllInternalIdsForExternalIds(externalIds),
        internalId,
        internalIds = [],
        internalIdsToMerge = [],
        isSuspicious = false;
    //second, create a list of all unique internal ids found
    for(var externalIdType in matchedExternalIdTypesToInternalIds) {
        if(matchedExternalIdTypesToInternalIds.hasOwnProperty(externalIdType)
            && internalIds.indexOf(matchedExternalIdTypesToInternalIds[externalIdType]['internalId']) < 0) {
            internalIds.push(matchedExternalIdTypesToInternalIds[externalIdType]['internalId']);
        }
    }
    //third, check the length of the list of internal ids found. if there is only one, just use it!
    //make sure that all external ids are assigned the same internal id
    if(internalIds.length === 1) {
        internalId = internalIds[0];
        dam.applyInternalIdToAllExternalIds(internalId,externalIds);
        isSuspicious = dam.isSuspicious(internalId);
    }
    //fourth, if there are no internal ids in the list, then we need to create one and assign it to all the external
    //ids we have
    else if(internalIds.length === 0) {
        internalId = dam.createAndApplyInernalIdToAllExternalIds(externalIds);
    }
    //fifth, if there are multiple internal ids in the list, then we need to resolve the right one to use, filling in
    //internal ids where appropriate and figuring out which ids, if any, need to merge
    else {
        //temp
        for(var i = 0 ; i < internalIds.length ; i++) {
            if(internalId && internalIdsToMerge.indexOf(internalId) < 0) {
                //TODO should the id to merge be marked as 'suspicious'?
                internalIdsToMerge.push(internalId);
            }
            if(!dam.isSuspicious(internalIds[i])) {
                internalId = internalIds[i];
            }
        }
    }
    return Q.fcall(function(){
        return {
            internalId: internalId,
            internalIdsToMerge: internalIdsToMerge,
            isSuspicious: isSuspicious
        };
    });
}

exports.getInternalId = getInternalUserId;