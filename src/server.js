//setup Dependencies
var express = require('express'),
    controller = require('./controller'),
    port = (process.env.PORT || 8081);

//TODO load and validate external ids configuration file

var server = express();

server.get('/getInternalId',function(req, res) {
    controller.getInternalId(req.query,function(internalId,internalIdsToMerge){
        res.json(200,{
            internalId: internalId,
            internalIdsToMerge: internalIdsToMerge
        });
    });
});

server.listen(port);
console.log('Listening on port ' + port);