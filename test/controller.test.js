var mockery = require('mockery'),
    controller;
mockery.registerSubstitute('./dam',__dirname + '/mock/dam');
mockery.registerAllowables(['../src/controller','../externalIdsConfig.json',__dirname + '/mock/dam','./mock/dam','q']);
//TODO: figure out why this doesn't work...
//mockery.enable({ useCleanCache: true });

exports.setUp = function (callback) {
    mockery.enable();
    mockDam = require('./mock/dam');
    mockDam.resetData();
    controller = require('../src/controller');
    callback();
};

exports.tearDown = function (callback) {
    mockery.disable();
    controller = null;
    callback();
};

exports.testGetInternalIdForUnknownUser = function(test) {
    test.expect(3);
    controller.getInternalId({cscookie: 'cookie1'})
        .then(function(obj){
            test.strictEqual(obj.internalId,'1','internal id for first unknown external id should be "1"');
            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
            test.done();
        })
        .done();
};

exports.testGetInternalIdsUpdated = function(test) {
    test.expect(6);
    controller.getInternalId({cscookie: 'cookie1',facebookId: 'facebookId1'})
        .then(function(obj){
            test.strictEqual(obj.internalId,'1','internal id for known external id should be "1"');
            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
            return controller.getInternalId({facebookId: 'facebookId1'});
        })
        .then(function(obj){
            test.strictEqual(obj.internalId,'1','internal id for known external id should be "1"');
            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
            test.done();
        })
        .done();
};

exports.testGetInternalIdsToMerge = function(test) {
    test.expect(5);
    controller.getInternalId({facebookId: 'facebookId1'})
        .then(function(obj){
            return controller.getInternalId({cscookie: 'cookie2'});
        })
        .then(function(obj){
            test.strictEqual(obj.internalId,'2','internal id for unknown external id should be "2"');
            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
            return controller.getInternalId({cscookie: 'cookie2',facebookId: 'facebookId1'});
        })
        .then(function(obj){
//            test.strictEqual(obj.internalId,'1','internal id for unknown external id should be "2"');
            test.strictEqual(obj.internalIdsToMerge.length,1,'there should be 1 to merge');
//            test.strictEqual(obj.internalIdsToMerge[0],'2','internal id "2" should be marked as needing to be merged');
            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
            test.done();
        })
        .done();
};

exports.testGetInternalIdWithMultipleUniques = function(test) {
    test.expect(5);
    controller.getInternalId({cscookie: 'cookie1'})
        .then(function(obj1){
            test.strictEqual(obj1.internalId,'1','internal id should be "1"');
            return controller.getInternalId({cscookie: 'cookie1', facebookId: 'facebookId1'});
        })
        .then(function(obj2){
            test.strictEqual(obj2.internalId,'1','internal id should be "1"');
            test.strictEqual(obj2.internalIdsToMerge.length,0,'there should be no internal ids to merge');
            test.strictEqual(obj2.isSuspicious,false,'should not be suspicious');
            return controller.getInternalId({cscookie: 'cookie1', facebookId: 'facebookId2'});
        })
        .then(function(obj3){
            test.strictEqual(obj3.internalId,'2','should use new internal id when non-unique id is used for multiple "unique" ids');
            test.done();
        })
        .done();
};

//exports.testGetInternalIdsWithSuspicious = function(test) {
//    test.expect(10);
//    controller.getInternalId({facebookId: 'facebookId1'})
//        .then(function(obj){
//            test.strictEqual(obj.internalId,'1','internal id should be "1"');
//            return controller.getInternalId({cscookie: 'cookie3',facebookId: 'facebookId2'});
//        })
//        .then(function(obj){
//            test.strictEqual(obj.internalId,'2','internal id for unknown external id should be "2"');
//            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
//            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
//            return controller.getInternalId({cscookie: 'cookie3',facebookId: 'facebookId1'});
//        })
//        .then(function(obj){
//            test.strictEqual(obj.internalId,'1','internal id for known external id should be "1"');
//            test.strictEqual(obj.internalIdsToMerge.length,1,'there should be 1 id to merge');
//            test.strictEqual(obj.isSuspicious,false,'should not be suspicious');
//            return controller.getInternalId({cscookie: 'cookie3'});
//        })
//        .then(function(obj){
//            test.strictEqual(obj.internalId,'2','internal id for known external id should be "2"');
//            test.strictEqual(obj.internalIdsToMerge.length,0,'there should be no ids to merge');
//            //TODO uncomment this guy after refactoring
//            test.strictEqual(obj.isSuspicious,true,'should be suspicious');
//            test.done();
//        })
//        .done();
//};