var _externalIdDefinitions = require('../externalIdsConfig.json'),
    counter,
    dataBase,
    suspiciousBase;

function resetData() {
    counter = 1;
    dataBase = {};
    suspiciousBase = {};
    for(var externalIdType in _externalIdDefinitions) {
        if(_externalIdDefinitions.hasOwnProperty(externalIdType)) {
            dataBase[externalIdType] = {};
        }
    }
}

resetData();

function findAllInternalIdsForExternalIds(externalIds) {
    var externalIdTypeToInternalIdMap = {};
    for(var externalIdType in externalIds) {
        if(externalIds.hasOwnProperty(externalIdType) && dataBase.hasOwnProperty(externalIdType)) {
            if(dataBase[externalIdType].hasOwnProperty(externalIds[externalIdType])) {
                externalIdTypeToInternalIdMap[externalIdType] = {
                    externalId: externalIds[externalIdType],
                    internalId: dataBase[externalIdType][externalIds[externalIdType]]
                };
            }
        }
    }
    return externalIdTypeToInternalIdMap;
}

function createAndApplyInernalIdToAllExternalIds(externalIds) {
    var internalId = '' + counter++;
    applyInternalIdToAllExternalIds(internalId,externalIds);
    return internalId;
}

function applyInternalIdToAllExternalIds(internalId,externalIds) {
    for(var externalIdType in externalIds) {
        if(externalIds.hasOwnProperty(externalIdType) && dataBase.hasOwnProperty(externalIdType)) {
            //if a value aleady exists, then mark it as suspicious
            //TODO is that really what we want? what about after a merge happens...
            if(dataBase[externalIdType].hasOwnProperty(externalIds[externalIdType])) {
                suspiciousBase[externalIds[externalIdType]] = 1;
            }
            dataBase[externalIdType][externalIds[externalIdType]] = internalId;
        }
    }
}

function isSuspicious(id) {
    return suspiciousBase.hasOwnProperty(id);
}

exports.findAllInternalIdsForExternalIds = findAllInternalIdsForExternalIds;
exports.createAndApplyInernalIdToAllExternalIds = createAndApplyInernalIdToAllExternalIds;
exports.applyInternalIdToAllExternalIds = applyInternalIdToAllExternalIds;
exports.isSuspicious = isSuspicious;

exports.resetData = resetData;